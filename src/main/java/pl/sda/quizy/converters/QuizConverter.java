package pl.sda.quizy.converters;

import org.springframework.stereotype.Component;
import pl.sda.quizy.dto.Answer;
import pl.sda.quizy.dto.Quiz;
import pl.sda.quizy.entities.Category;
import pl.sda.quizy.entities.Question;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class QuizConverter {

	public Quiz getQuiz(Category category) {
		Quiz quizDTO = new Quiz();
		quizDTO.setCategory(category.getName());

		quizDTO.setQuestions(new ArrayList<>());

		category.getQuestions().stream().forEach( q->{

			pl.sda.quizy.dto.Question questionDTO = new pl.sda.quizy.dto.Question();
			questionDTO.setId(q.getId());
			questionDTO.setText(q.getText());

			q.getAnswerSet().getAnswers().stream().forEach( a -> {
				pl.sda.quizy.dto.Answer answerDTO = new pl.sda.quizy.dto.Answer();

				answerDTO.setId(a.getId());
				answerDTO.setText(a.getText());

				questionDTO.getAnswers().add(answerDTO);
			});

			quizDTO.getQuestions().add(questionDTO);
		});

		return quizDTO;
	}
}
