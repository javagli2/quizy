package pl.sda.quizy.controllers;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class HomeController {

	@GetMapping(value = "/home")
	public String home(ModelMap model, @AuthenticationPrincipal User user){

		Set<String> roles = user.getAuthorities().stream().map(a -> a.getAuthority()).collect(Collectors.toSet());

		model.addAttribute("username", user.getUsername());
		model.addAttribute("roles", roles);

		return "home";
	}
}
