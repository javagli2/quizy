package pl.sda.quizy.repositories.custom;

import pl.sda.quizy.entities.Category;
import pl.sda.quizy.entities.Question;

import java.util.List;

public interface QuestionRepositoryCustom {
	List<Question> findByCategory(String category);

	List<Question> findByCategory(Category category);
}
