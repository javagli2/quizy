package pl.sda.quizy.repositories.custom;

import pl.sda.quizy.entities.Category;

public interface CategoryRepositoryCustom {
	Category findByName(String categoryName);
}
