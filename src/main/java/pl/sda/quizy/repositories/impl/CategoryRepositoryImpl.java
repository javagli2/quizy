package pl.sda.quizy.repositories.impl;

import pl.sda.quizy.entities.Category;
import pl.sda.quizy.repositories.custom.CategoryRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class CategoryRepositoryImpl implements CategoryRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Category findByName(String categoryName) {
		Query query = entityManager.createQuery("select c from Category as c where c.name = :categoryName");
		query.setParameter("categoryName", categoryName);

		return (Category) query.getSingleResult();
	}
}
