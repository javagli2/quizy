package pl.sda.quizy.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sda.quizy.entities.AnswerSet;
import pl.sda.quizy.entities.Question;

import javax.annotation.Resource;

@Resource(name = "answerSetRepository")
public interface AnswerSetRepository extends CrudRepository<AnswerSet, Integer> {
}
