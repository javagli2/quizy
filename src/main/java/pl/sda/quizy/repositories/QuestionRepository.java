package pl.sda.quizy.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sda.quizy.entities.Question;
import pl.sda.quizy.repositories.custom.QuestionRepositoryCustom;

import javax.annotation.Resource;

@Resource(name = "questionRepository")
public interface QuestionRepository extends CrudRepository<Question, Integer>, QuestionRepositoryCustom {
}
