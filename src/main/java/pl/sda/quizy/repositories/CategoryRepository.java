package pl.sda.quizy.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sda.quizy.entities.Category;
import pl.sda.quizy.repositories.custom.CategoryRepositoryCustom;

import javax.annotation.Resource;

@Resource(name = "categoryRepository")
public interface CategoryRepository extends CrudRepository<Category, Integer>, CategoryRepositoryCustom {
}
