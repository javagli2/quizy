package pl.sda.quizy;

import org.springframework.stereotype.Component;
import pl.sda.quizy.entities.Answer;
import pl.sda.quizy.entities.AnswerSet;
import pl.sda.quizy.entities.Category;
import pl.sda.quizy.entities.Question;
import pl.sda.quizy.repositories.CategoryRepository;
import pl.sda.quizy.repositories.QuestionRepository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestData {

    @Resource(name = "categoryRepository")
    private CategoryRepository categoryRepository;

    @Resource(name = "questionRepository")
    private QuestionRepository questionRepository;

    public void addQuizPawla() {
        Category category = new Category();
        category.setName("Astronomia");

        Question question1 = createQuestion(category,
                "Kto wstrzymał Słońce, a ruszył Ziemię?",
                "Mikołaj Kopernik",
                "Galileo Galilei",
                "Jan Heweliusz",
                "Edwin Hubble"
        );

        Question question2 = createQuestion(category,
                "Która planeta nie ma naturalnych satelitów?",
                "Merkury",
                "Jowisz",
                "Mars",
                "Uran"
        );

        List<Question> questionsList = new ArrayList<>();
        questionsList.add(question1);
        questionsList.add(question2);

        questionRepository.saveAll(questionsList);
    }

    public void addQuizMarcina() {

        Category category = new Category();
        category.setName("Matematyka");

        Question question1 = createQuestion(category, "Liczba PI w przybliżeniu to: ",
                "3,141592",
                "3,15",
                "3,143",
                "3,13");
        Question question2 = createQuestion(category, "Wzór na pole powierzchni trójkąta prostokątnego: ",
                "P = 1/2a * h",
                "P = (a * b * C)/4R",
                "P = (a * 2) + (b * 2) + (c * 2 )",
                "P = p(p−a)(p−b)(p−c)");

        Question question3 = createQuestion(category, "Czyje jest to twierdzenie: W trójkącie prostokątnym, " +
                        "suma kwadratów przyprostokątnych jest równa kwadratowi przeciwprostokątnej",
                "Pitagoras",
                "Tales z Miletu",
                "Albert Einstein"
                );

        List<Question> questionList = new ArrayList<>();
        questionList.add(question1);
        questionList.add(question2);
        questionList.add(question3);

        questionRepository.saveAll(questionList);

    }

    public void addQuizWiktorii() {
        Category category = new Category();
        category.setName("Chemia");

        Question question1 = createQuestion(category, "Kto odkrył Rad?",
                "Maria Skłodowska-Curie i Pierre Curie",
                "Dmitrij Mendelejew",
                "Ernest Rutherford",
                "Niels Bohr"
        );

        Question question2 = createQuestion(category, "P to symbol chemiczny:",
                "fosforu",
                "potasu",
                "chloru",
                "polonu"
        );

        List<Question> questionsList = new ArrayList<>();
        questionsList.add(question1);
        questionsList.add(question2);

        questionRepository.saveAll(questionsList);
    }



    private Question createQuestion(Category category, String questionText, String correctAnswerText, String... wrongAnswerTexts) {
        Question question = createQuestion(category, questionText);

        createAnswer(question, correctAnswerText, true);

        for (String wrongAnswerText : wrongAnswerTexts) {
            createAnswer(question, wrongAnswerText, false);
        }

        return question;
    }

    private Question createQuestion(Category category, String questionText) {
        Question question = new Question();
        question.setText(questionText);

        question.setCategory(category);
        question.setAnswerSet(new AnswerSet());
        return question;
    }

    private void createAnswer(Question question, String wrongAnswerText, boolean b) {
        Answer wrongAnswer = new Answer();
        wrongAnswer.setText(wrongAnswerText);
        wrongAnswer.setCorrect(b);
        wrongAnswer.setAnswerSet(question.getAnswerSet());
        question.getAnswerSet().getAnswers().add(wrongAnswer);
    }
}